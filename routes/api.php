<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\BoardController;
use App\Http\Controllers\Api\ListController;
use App\Http\Controllers\Api\TaskController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/


Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::get('board', [BoardController::class, 'index']);
});


Route::controller(AuthController::class)->group(function(){
    Route::post('register', 'register');
    Route::post('login', 'login');

    Route::get('list',[ListController::class,'index']);
    Route::get('list/{id}',[ListController::class,'show']);
    Route::post('list',[ListController::class,'store']);
    Route::put('list/{id}',[ListController::class,'update']);
    Route::delete('list/{id}',[ListController::class,'destroy']);

    Route::get('task',[TaskController::class,'index']);
    Route::get('task/{id}',[TaskController::class,'show']);
    Route::post('task',[TaskController::class,'store']);
    Route::put('task/{id}',[TaskController::class,'update']);
    Route::delete('task/{id}',[TaskController::class,'destroy']);

});
