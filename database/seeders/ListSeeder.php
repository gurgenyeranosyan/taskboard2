<?php

namespace Database\Seeders;

use App\Models\BoardList;
use Database\Factories\ListFactory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ListSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        BoardList::factory()->count(30)->create();
    }
}
