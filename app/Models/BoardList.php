<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;

class BoardList extends Model
{
    use HasFactory;

    protected $table = 'lists';

    protected $hidden = ['board_id'];

    protected $fillable = ['title', 'description'];

    public function tasks(): HasMany
    {
        return $this->hasMany(Task::class, 'list_id', 'id');
    }

    public function user(): HasManyThrough
    {
        return $this->hasManyThrough(Board::class, User::class, 'id', 'user_id', 'board_id', 'id');
    }

    public function board(): BelongsTo
    {
        return $this->belongsTo(Board::class, 'board_id', 'id');
    }
}
