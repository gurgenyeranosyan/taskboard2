<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;

class Task extends Model
{
    use HasFactory;

    protected $hidden = ['list_id'];

    public function board(): HasManyThrough
    {
        return $this->hasManyThrough(BoardList::class, Board::class, 'id', 'board_id', 'list_id', 'id');
    }

    public function list(): BelongsTo
    {
        return $this->belongsTo(BoardList::class, 'list_id', 'id');
    }


    public function tags(): BelongsToMany
    {
        return $this->belongsToMany(Tag::class, 'task_tag', 'task_id', 'tag_id');
    }
}
