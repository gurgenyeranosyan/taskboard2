<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ListRequest;
use App\Http\Resources\ListResource;
use App\Http\ServerResponse\ServerResponse;
use App\Http\Services\ListService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class ListController extends Controller
{


    private $listService;

    public function __construct(ListService $listService)
    {
        $this->listService = $listService;
    }

    /**
     * Display a listing of the resource.
     */
    public function index(): JsonResponse
    {

        $result = ServerResponse::RESPONSE_200;
        try {
            $result['data'] = ListResource::collection($this->listService->index());
        } catch (ModelNotFoundException $e) {
            $result = ServerResponse::RESPONSE_404;
        } catch (\Exception $e) {
            $result = ServerResponse::RESPONSE_500;
        }

        return response()->json($result, $result['status']);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(ListRequest $request): JsonResponse
    {
        $result = ServerResponse::RESPONSE_201;
        try {
            $result['data'] = new ListResource($this->listService->store($request->validated()));
        } catch (ModelNotFoundException $e) {
            $result = ServerResponse::RESPONSE_404;
        } catch (\Exception $e) {
            dd($e->getMessage());
            $result = ServerResponse::RESPONSE_500;
        }

        return response()->json($result, $result['status']);
    }

    /**
     * Display the specified resource.
     */
    public function show(int $id): JsonResponse
    {
        $result = ServerResponse::RESPONSE_200;
        try {
            $result['data'] = new ListResource($this->listService->show($id));
        } catch (ModelNotFoundException $e) {
            $result = ServerResponse::RESPONSE_404;
        } catch (\Exception $e) {
            $result = ServerResponse::RESPONSE_500;
        }

        return response()->json($result, $result['status']);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(ListRequest $request, int $id): JsonResponse
    {
        $result = ServerResponse::RESPONSE_200;
        try {
            $result['data'] = new ListResource($this->listService->updateList($request->validated(), $id));
        } catch (ModelNotFoundException $e) {
            $result = ServerResponse::RESPONSE_404;
        } catch (\Exception $e) {
            $result = ServerResponse::RESPONSE_500;
        }

        return response()->json($result, $result['status']);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $result = ServerResponse::RESPONSE_200;
        try {
            $this->listService->deleteList($id);
        } catch (ModelNotFoundException $e) {
            $result = ServerResponse::RESPONSE_404;
        } catch (\Exception $e) {
            $result = ServerResponse::RESPONSE_500;
        }

        return response()->json($result, $result['status']);
    }
}
