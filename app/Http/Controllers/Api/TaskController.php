<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\TaskRequest;
use App\Http\Resources\TasksResource;
use App\Http\ServerResponse\ServerResponse;
use App\Http\Services\ListService;
use App\Http\Services\TaskService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class TaskController extends Controller
{


    private $taskService;
    private $listService;

    public function __construct(TaskService $taskService,ListService $listService)
    {
        $this->taskService = $taskService;
        $this->listService = $listService;
    }

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $result = ServerResponse::RESPONSE_200;
        try {
            $result['data'] = TasksResource::collection($this->taskService->index($request->all()));
        } catch (ModelNotFoundException $e) {
            $result = ServerResponse::RESPONSE_404;
        } catch (\Exception $e) {
            dd($e->getMessage());
            $result = ServerResponse::RESPONSE_500;
        }

        return response()->json($result, $result['status']);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(TaskRequest $request)
    {

        $result = ServerResponse::RESPONSE_201;
        try {
            $this->listService->show($request->list_id);
            $result['data'] = new TasksResource($this->taskService->store($request->all()));
        } catch (ModelNotFoundException $e) {
            $result = ServerResponse::RESPONSE_404;
        } catch (\Exception $e) {
            dd($e->getMessage());
            $result = ServerResponse::RESPONSE_500;
        }

        return response()->json($result, $result['status']);
    }

    /**
     * Display the specified resource.
     */
    public function show(int $id)
    {
        $result = ServerResponse::RESPONSE_200;
        try {
            $result['data'] = new TasksResource($this->taskService->show($id));
        } catch (ModelNotFoundException $e) {
            $result = ServerResponse::RESPONSE_404;
        } catch (\Exception $e) {
            $result = ServerResponse::RESPONSE_500;
        }

        return response()->json($result, $result['status']);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(TaskRequest $request, string $id)
    {
        $result = ServerResponse::RESPONSE_200;
        try {
            $this->listService->show($request->list_id);
            $result['data'] = new TasksResource($this->taskService->updateTask($request->validated(), $id));
        } catch (ModelNotFoundException $e) {
            $result = ServerResponse::RESPONSE_404;
        } catch (\Exception $e) {
            dd($e->getMessage());
            $result = ServerResponse::RESPONSE_500;
        }

        return response()->json($result, $result['status']);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $result = ServerResponse::RESPONSE_200;
        try {
            $this->taskService->deleteTask($id);
        } catch (ModelNotFoundException $e) {
            $result = ServerResponse::RESPONSE_404;
        } catch (\Exception $e) {
            $result = ServerResponse::RESPONSE_500;
        }

        return response()->json($result, $result['status']);
    }
}
