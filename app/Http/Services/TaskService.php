<?php


namespace App\Http\Services;

use App\Models\Task;
use Illuminate\Database\Eloquent\Collection;

class TaskService extends BaseService
{
    /**
     * @return string
     */
    protected function model(): string
    {
        return 'App\Models\Task';
    }

    /**
     * @return Collection
     */
    public function index(array $request): Collection
    {

        $deadline_start = $request['deadline_start'] ?? false;
        $deadline_end = $request['deadline_end'] ?? false;
        $deadline_order = $request['deadline_order'] ?? false;

        $tasks = $this->query->whereHas('board', function ($boardQuery) {
            $boardQuery->whereHas('user', function ($userQuery) {
                $userQuery->where('users.id', auth('sanctum')->user()->id);
            });
        })->when($deadline_start ||$deadline_end, function ($query) use ($deadline_start, $deadline_end) {
            return $query
                ->when($deadline_start && $deadline_end, function ($query) use ($deadline_start, $deadline_end) {
                    return $query->whereBetween('deadline', [$deadline_start, $deadline_end]);
                })
                ->when(isset($request['deadline_start']) && !isset($request['deadline_end']), function ($query) use ($deadline_start) {
                    return $query->where('deadline', '>=', $deadline_start);
                })
                ->when(!isset($request['deadline_start']) && isset($request['deadline_end']), function ($query) use ($deadline_end) {
                    return $query->where('deadline', '<=', $deadline_end);
                });
        })
            ->when($deadline_order && ($deadline_order == 'asc' || $deadline_order == 'desc'), function ($query) use ($deadline_order) {
                return $query->orderBy('deadline',$deadline_order);
            })
            ->when(!$deadline_order, function ($query) {
                return $query->orderBy('order');
            })
            ->get();
        return $tasks;
    }

    public function store(array $request): Task
    {
        $task = $this->create($request, ['title', 'description', 'deadline', 'order']);
        $task->list()->associate($request['list_id'])->save();

        if(isset($request['tags'])){
            $task->tags()->sync(json_decode($request['tags']));
        }

        return $task;
    }

    public function show(int $id): Task
    {
        $task = $this->query->findOrFail($id);
        return $task;
    }

    public function updateTask(array $request, int $id): Task
    {
        $task = $this->update($request, $id, ['title', 'description', 'deadline', 'order']);
        $task->list()->associate($request['list_id'])->save();
        if(isset($request['tags'])){
            $task->tags()->sync(json_decode($request['tags']));
        }
        return $task;
    }

    public function deleteTask(int $id): void
    {
        $this->delete($id);
    }

}
