<?php


namespace App\Http\Services;

use App\Models\BoardList;
use Illuminate\Database\Eloquent\Collection;

class ListService extends BaseService
{
    /**
     * @return string
     */
    protected function model(): string
    {
        return 'App\Models\BoardList';
    }

    /**
     * @return Collection
     */
    public function index(): Collection
    {
        $lists = $this->query->whereHas('user', function ($userQuery) {
            $userQuery->where('users.id', auth('sanctum')->user()->id);
        })->orderBy('order')->get();
        return $lists;
    }

    public function store(array $request): BoardList
    {

        $list = $this->create($request, ['title', 'description', 'order']);
        $list->board()->associate(auth('sanctum')->user()->board->id)->save();
        return $list;
    }

    public function show(int $id): BoardList
    {
        $list = $this->query->findOrFail($id);
        return $list;
    }

    public function updateList(array $request, int $id): BoardList
    {
        $list = $this->update($request, $id, ['title', 'description', 'order']);
        return $list;
    }

    public function deleteList(int $id): void
    {
        $this->delete($id);
    }
}
