<?php


namespace App\Http\Services;
use App\Models\Board;
use Illuminate\Database\Eloquent\Collection;

class BoardService extends BaseService
{
    /**
     * @return string
     */
    protected function model(): string
    {
        return 'App\Models\Board';
    }

    /**
     * @return Collection
     */
    public function index(): Board
    {
        $board = $this->query->select('id', 'title', 'description')->with(['boardList' => function($listsQuery){
            $listsQuery->select('id', 'title', 'description', 'board_id')->with(['tasks' => function($tasksQuery){
                $tasksQuery->select('id', 'title', 'description', 'order', 'list_id');
            }]);
        }])->whereHas('user', function ($user){
            $user->where('users.id', auth('sanctum')->user()->id);
        })->first();

        return $board;
    }
}
