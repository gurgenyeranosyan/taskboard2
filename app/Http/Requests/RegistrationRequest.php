<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\JsonResponse;

class RegistrationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'email' => 'required|email|unique:users',
            'name' => 'required',
            'surname' => 'required',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ];
    }

    public function messages()
    {
        return [
            'email.required' => 'Поле электронной почты обязателен для заполнения',
            'email.unique' => 'Электронная почта уже занята.',
            'email.email' => 'Недопустимый формат электронной почты',
            'password.required' => 'Поле пароля почты обязателен для заполнения',
            'password.unique' => 'Поле пароля бязателен для заполнения.',
            'c_password.required' => 'Поле повторение пароля обязателен для заполнения.',
            'c_password.same' => 'Пароль и повторение пароля должны совпадать.',
            'name.required' => 'Поле имя обязателен для заполнения.',
            'surname.required' => 'Поле фамилия обязателен для заполнения.',
        ];
    }

    public function failedValidation(Validator $validator): JsonResponse
    {
        throw new HttpResponseException(response()->json([
            'success' => false,
            'data' => $validator->errors()
        ], 500));
    }
}
